package org.asmolinski.letsplayjunit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class NonNestedTests {

  @Mock
  private Precondition precondition;

  @Mock
  private DoCheckIn doCheckIn;

  @InjectMocks
  private CheckInService testedObject;

  @BeforeEach
  void setUp() {
    given(doCheckIn.doCheckIn(any())).willReturn("SUCCESS");
  }

  @Test
  void shouldThrowOnPreconditionFailOutward() {
    given(precondition.check()).willReturn(false);
    assertThrows(IllegalStateException.class, () -> testedObject.checkInOutward());
  }

  @Test
  void shouldThrowOnPreconditionFailReturn() {
    given(precondition.check()).willReturn(false);
    assertThrows(IllegalStateException.class, () -> testedObject.checkInReturn());
  }

  @Test
  void shouldCheckInOutward() {
    // given
    given(precondition.check()).willReturn(true);
    // when
    final String actual = testedObject.checkInOutward();
    // then
    assertThat(actual).isEqualTo("SUCCESS");
  }

  @Test
  void shouldCheckInReturn() {
    // given
    given(precondition.check()).willReturn(true);
    // when
    final String actual = testedObject.checkInReturn();
    // then
    assertThat(actual).isEqualTo("SUCCESS");
  }
}
