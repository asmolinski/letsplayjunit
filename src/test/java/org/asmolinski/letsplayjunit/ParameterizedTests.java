package org.asmolinski.letsplayjunit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class ParameterizedTests {

  @ParameterizedTest
  @MethodSource("getParameters")
  void shouldSort(final List<String> input, final List<String> expected) {
    System.out.println(input);

    final var actual = input.stream().sorted().collect(toList());

    assertThat(actual).isEqualTo(expected);
  }

  private static Stream<Arguments> getParameters() {
    final var sorted = List.of("aaa", "bbb", "ccc");
    final var reverseSorted = List.of("ccc", "bbb", "aaa");
    final var unsorted = List.of("aaa", "ccc", "bbb");
    final var empty = List.of();
    return Stream.of(
        arguments(empty, empty),
        arguments(sorted, sorted),
        arguments(reverseSorted, sorted),
        arguments(unsorted, sorted)
    );
  }

  @ParameterizedTest
  @EnumSource(ParameterizedTests.MyEnum.class)
  void shouldInvokeForEachEnumValue(final MyEnum input) {
    assertThat(input.toString()).isEqualTo("MyEnum{" + input.name() + "}");
  }

  public enum MyEnum {
    FOO, BAR, BAZ;

    @Override
    public String toString() {
      return "MyEnum{" + name() + "}";
    }

  }
}
