package org.asmolinski.letsplayjunit;

public interface DoCheckIn {
  String doCheckIn(final String direction);
}
