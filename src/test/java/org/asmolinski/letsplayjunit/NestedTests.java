package org.asmolinski.letsplayjunit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class NestedTests {

  @Mock
  private Precondition precondition;

  @Mock
  private DoCheckIn doCheckIn;

  @InjectMocks
  private CheckInService testedObject;

  @BeforeEach
  void setUp() {
    System.out.println("Hello from parent");
  }

  @Nested
  class NegativeScenarios {
    @BeforeEach
    void setUp() {
      System.out.println("Hello from nested 1");
      given(precondition.check()).willReturn(false);
    }

    @Test
    void shouldThrowOnPreconditionFailOutward() {
      assertThrows(IllegalStateException.class, () -> testedObject.checkInOutward());
    }

    @Test
    void shouldThrowOnPreconditionFailReturn() {
      assertThrows(IllegalStateException.class, () -> testedObject.checkInReturn());
    }

  }

  @Nested
  class PositiveScenarios {
    @BeforeEach
    void setUp() {
      System.out.println("Hello from nested 2");
      given(precondition.check()).willReturn(true);
      given(doCheckIn.doCheckIn(any())).willReturn("SUCCESS");
    }

    @Test
    void shouldCheckInOutward() {
      // when
      final String actual = testedObject.checkInOutward();
      // then
      assertThat(actual).isEqualTo("SUCCESS");
    }

    @Test
    void shouldCheckInReturn() {
      // when
      final String actual = testedObject.checkInReturn();
      // then
      assertThat(actual).isEqualTo("SUCCESS");
    }
  }
}
