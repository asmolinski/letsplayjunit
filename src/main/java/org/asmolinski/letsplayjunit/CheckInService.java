package org.asmolinski.letsplayjunit;

class CheckInService {
  final Precondition precondition;
  final DoCheckIn doCheckIn;

  CheckInService(final Precondition precondition,
      final DoCheckIn doCheckIn) {
    this.precondition = precondition;
    this.doCheckIn = doCheckIn;
  }

  String checkInOutward() {
    if (!precondition.check()) {
      throw new IllegalStateException();
    }
    return doCheckIn.doCheckIn("outward");
  }

  String checkInReturn() {
    if (!precondition.check()) {
      throw new IllegalStateException();
    }
    return doCheckIn.doCheckIn("return");
  }
}
