package org.asmolinski.letsplayjunit;

class MyVerySpecialException extends Exception {
  private final String errorCode;

  MyVerySpecialException(final String message, final String errorCode) {
    super(message);
    this.errorCode = errorCode;
  }

  String getErrorCode() {
    return errorCode;
  }
}
