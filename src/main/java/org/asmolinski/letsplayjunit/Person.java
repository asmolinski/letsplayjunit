package org.asmolinski.letsplayjunit;

public class Person {
  private final String firstName;
  private final String lastName;
  private final int age;

  public Person(final String firstName, final String lastName, final int age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public int getAge() {
    return age;
  }
}
