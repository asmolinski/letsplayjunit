package org.asmolinski.letsplayjunit;

class ExceptionThrower {
  public void fire() throws MyVerySpecialException {
    throw new MyVerySpecialException("message", "ERR123");
  }
}
