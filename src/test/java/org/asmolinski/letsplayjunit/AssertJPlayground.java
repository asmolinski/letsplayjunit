package org.asmolinski.letsplayjunit;

import org.assertj.core.api.Condition;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.data.Index.atIndex;
import static org.assertj.core.groups.Tuple.tuple;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AssertJPlayground {

  @Test
  void differentEqualities() {
    final var original = new ValueHolder("somevalue");
    final var differentUuid = new ValueHolder("somevalue");
    final var sameInstance = original;

    assertThat(original).isEqualTo(sameInstance) // equals
        .isSameAs(sameInstance) // ==
        .isNotEqualTo(differentUuid);
    assertThat(original)
        .isEqualToComparingOnlyGivenFields(differentUuid, "value")
        .isEqualToIgnoringGivenFields(differentUuid, "uuid");
  }

  @Test
  void differentEqualitiesPart2() {
    final var ten = BigDecimal.valueOf(10);
    final var alsoTen = BigDecimal.valueOf(10.00);
    assertThat(ten)
        .isNotEqualTo(alsoTen)
        .isEqualByComparingTo(alsoTen);
  }

  @Test
  void collectionsBasics() {
    final var myList = List.of("Narwhal", "Walrus", "Dugong", "Seal");
    assertThat(myList).contains("Narwhal").doesNotContain("Fish");
    assertThat(myList).contains("Dugong", atIndex(2));
    assertThat(myList).containsAnyOf("Dugong", "Fish");
    assertThat(myList).isNotEmpty().hasSize(4).hasSizeGreaterThan(2);

    final var myListWithRepetitions = List.of("we will", "we will", "rock you");
    assertThat(myListWithRepetitions).containsOnly("we will", "rock you");
    assertThat(myListWithRepetitions).containsExactly("we will", "we will", "rock you");
    assertThat(myListWithRepetitions).containsExactlyInAnyOrder("we will", "rock you", "we will");
  }

  @Test
  void collectionsAdvanced() {
    final var dtoList = List.of(new Person("John", "Doe", 30),
        new Person("Jane", "Doe", 25), new Person("Winston", "Smith", 40));
    assertThat(dtoList)
        .extracting(Person::getAge)
        .containsExactly(30, 25, 40);
    // pre java 8 way - DO NOT USE!
    assertThat(dtoList)
        .extracting("age")
        .containsExactly(30, 25, 40);
    assertThat(dtoList)
        .extracting(Person::getFirstName, Person::getLastName)
        .contains(tuple("John", "Doe"));
    assertThat(dtoList)
        .filteredOn(p -> p.getLastName().equals("Doe"))
        .extracting(Person::getFirstName)
        .containsOnly("John", "Jane");
    assertThat(dtoList)
        .areExactly(2, hasAgeDivisibleByTen());
    assertThat(dtoList)
        .filteredOn(hasAgeDivisibleByTen())
        .extracting(Person::getFirstName)
        .containsExactly("John", "Winston");
  }

  private Condition<? super Person> hasAgeDivisibleByTen() {
    return new Condition<>(person -> person.getAge() % 10 == 0, "has age divisible by 10");
  }

  @Test
  public void shouldCatchThrowable() {
    // when
    final Throwable thrown = catchThrowable(() -> new ExceptionThrower().fire());
    // then
    assertThat(thrown).hasMessage("message");
    assertThat(thrown).isInstanceOfSatisfying(MyVerySpecialException.class,
        mvse -> assertThat(mvse.getErrorCode()).isEqualTo("ERR123"));
  }

  @Test
  public void shouldAssertThrows() {
    // when
    final Executable executable = () -> new ExceptionThrower().fire();
    // then
    final MyVerySpecialException actual = assertThrows(MyVerySpecialException.class, executable);
    assertThat(actual.getErrorCode()).isEqualTo("ERR123");
  }
}
