package org.asmolinski.letsplayjunit;

interface Precondition {
  boolean check();
}
