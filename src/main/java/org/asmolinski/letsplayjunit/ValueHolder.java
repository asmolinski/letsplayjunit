package org.asmolinski.letsplayjunit;

import java.util.UUID;

public class ValueHolder {
  final String value;
  final String uuid;

  public ValueHolder(final String value) {
    this.value = value;
    this.uuid = UUID.randomUUID().toString();
  }
}
