package org.asmolinski.letsplayjunit;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
    //@TestMethodOrder(MethodOrderer.Alphanumeric.class)
    //@TestMethodOrder(MethodOrderer.Random.class)
class OrderedTests {

  @Order(3)
  @Test
  void bTest() {
    System.out.println("Hello bTest");
  }

  @Order(1)
  @Test
  void cTest() {
    System.out.println("Hello cTest");
  }

  @Order(2)
  @Test
  void dTest() {
    System.out.println("Hello dTest");
  }

  @Order(4)
  @Test
  void aTest() {
    System.out.println("Hello aTest");
  }
}
